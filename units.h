#pragma once

#include "unittag.h"

#define RELAX_OPERATIONS

namespace units
{
    template<class Tag, class DataType = float>
    struct Unit
    {
        using TagType = Tag;
        DataType value;
        // All constructor are left by default !
        
        template<class Tag2>
        static constexpr bool is_same_unit()
        {
            return unittag::is_same_unittag(Tag{}, Tag2{});
        }

        static constexpr bool is_unitless()
        {
            return std::is_same_v<Tag, unittag::UnitTag<unittag::NoTag>>;
        }

        template<class Tag2, class DataType2>
        static constexpr bool is_same_unit_as(const Unit<Tag2, DataType2>&)
        {
            return is_same_unit<Tag2>();
        }

        template<class Tag2, class DataType2>
        static constexpr bool is_compatible()
        {
            return is_same_unit<Tag2>() && std::is_convertible_v<DataType2, DataType>;
        }

        template<class Tag2, class DataType2>
        static constexpr bool is_compatible(const Unit<Tag2, DataType2>&)
        {
            return is_compatible<Tag2, DataType2>();
        }
 
        constexpr Unit() : value(DataType()) {}
    
        template<class DataType2>
        constexpr Unit(const DataType2& val) : 
            value(static_cast<DataType>(val))
        { }

        template<class Tag2, class DataType2>
        constexpr Unit(const Unit<Tag2, DataType2>& other)
        { 
            static_assert(is_compatible(other), "Can not copy between different units");
            value = static_cast<DataType>(other.value);
        }

        template<class Tag2, class DataType2>
        constexpr auto operator=(const Unit<Tag2, DataType2>& other)
        {
            static_assert(is_compatible(other), "Can not copy between different units");
            value = static_cast<DataType>(other);
            return *this;
        }

        template<class Tag2, class DataType2>
        constexpr auto operator==(const Unit<Tag2, DataType2>& other)
        {
            static_assert(is_compatible(other), "Can not compare different units");
            return value == static_cast<DataType>(other.value);
        }

        template<class Tag2, class DataType2>
        constexpr auto operator!=(const Unit<Tag2, DataType2>& other)
        {
            return !(*this == other);
        }

        template<class Tag2, class DataType2>
        constexpr auto operator>(const Unit<Tag2, DataType2>& other)
        {            
            static_assert(is_compatible(other), "Can not compare different units");
            return value > static_cast<DataType>(other.value);
        }

        template<class Tag2, class DataType2>
        constexpr auto operator>=(const Unit<Tag2, DataType2>& other)
        {            
            return (*this > other) || (*this == other);
        }

        template<class Tag2, class DataType2>
        constexpr auto operator<=(const Unit<Tag2, DataType2>& other)
        {            
            return !(*this > other);
        }

        template<class Tag2, class DataType2>
        constexpr auto operator<(const Unit<Tag2, DataType2>& other)
        {            
            return !(*this >= other);
        }
        
        constexpr auto operator-()
        {
            return Unit<Tag, DataType>(-value);
        }

        template<class Tag2, class DataType2>
        constexpr auto operator+=(const Unit<Tag2, DataType2>& other)
        {            
            static_assert(is_compatible(other), "Can not add different units");
            value += static_cast<DataType>(other.value);
            return *this;
        }

        template<class Tag2, class DataType2>
        constexpr auto operator-=(const Unit<Tag2, DataType2>& other)
        {            
            static_assert(is_compatible(other), "Can not sub different units");
            value -= static_cast<DataType>(other.value);
            return *this;
        }

        template<class Tag2, class DataType2>
        constexpr auto operator+(const Unit<Tag2, DataType2>& other)
        {
            Unit<Tag, DataType> return_value(*this);
            return_value += other;
            return return_value;
        }

        template<class Tag2, class DataType2>
        constexpr auto operator-(const Unit<Tag2, DataType2>& other)
        {
            Unit<Tag, DataType> return_value(*this);
            return_value -= other;
            return return_value;
        }
        
        constexpr auto operator*=(const DataType& other)
        {
            value *= other;
            return *this;
        }

        constexpr auto operator*(const DataType& other)
        {
            Unit<Tag, DataType> return_value(*this);
            return_value *= other.value;
            return return_value;
        }

        template<class Tag2, class DataType2>
        constexpr auto operator*(const Unit<Tag2, DataType2>& other)
        {
            using return_type = decltype(unittag::simplified_unittag(unittag::combine_unittags(Tag{}, Tag2{})));
            return Unit<return_type, DataType> { value * static_cast<DataType>(other.value) };
        } 

        template<class Tag2, class DataType2>
        constexpr auto operator/(const Unit<Tag2, DataType2>& other)
        {
            using return_type = decltype(
                unittag::simplified_unittag(
                    unittag::combine_unittags(
                        Tag{}, unittag::inverse_unittag(Tag2{})
                    )
                )
            );
            return Unit<return_type, DataType> { value / static_cast<DataType>(other.value) };
        }       

        template<class Stream>
        friend constexpr Stream& operator<<(Stream& s, const Unit<Tag, DataType>& other)
        {
            if constexpr (!std::is_same_v<Tag, unittag::UnitTag<unittag::NoTag>>) s << "[";
            unittag::unit_print(s, Tag{});
            if constexpr (!std::is_same_v<Tag, unittag::UnitTag<unittag::NoTag>>) s << "]";
            s << other.value;
            return s;
        }
        
        // Operation that should not be allowed, but forbinding them
        // would be harmfull to code simplicity
        // - Setting value with a constant
        // - Adding with a non unit constant
        // - Comparing with a non unit constant
        #ifdef RELAX_OPERATIONS
            constexpr auto operator=(const DataType &other)
            {
                value = other;
                return *this;
            }

            constexpr auto operator+=(const DataType& other)
            {
                value += other;
                return *this;
            }

            constexpr auto operator-=(const DataType& other)
            {
                value -= other;
                return *this;
            }
            
            constexpr auto operator+(const DataType& other)
            {
                Unit<Tag, DataType> return_value(*this);
                return_value += other;
                return return_value;
            }

            template<class DataType2>
            constexpr auto operator+(const Unit<unittag::UnitTag<unittag::NoTag>, DataType>& other)
            {
                Unit<Tag, DataType> return_value(*this);
                return_value += static_cast<DataType>(other.value);
                return return_value;
            }

            constexpr auto operator-(const DataType& other)
            {
                Unit<Tag, DataType> return_value(*this);
                return_value -= other;
                return return_value;
            }

            template<class DataType2>
            constexpr auto operator-(const Unit<unittag::UnitTag<unittag::NoTag>, DataType2>& other)
            {
                Unit<Tag, DataType> return_value(*this);
                return_value -= static_cast<DataType>(other.value);
                return return_value;
            }

            constexpr auto operator==(const Unit<unittag::UnitTag<unittag::NoTag>, DataType>& other)
            {
                return value == other.value;
            }

            constexpr auto operator==(const DataType& other)
            { 
                return value == other.value; 
            }

            constexpr auto operator!=(const DataType& other)
            {
                return !(*this == other);
            }

            constexpr auto operator>(const DataType& other)
            {            
                return value > other.value;
            }

            constexpr auto operator>=(const DataType& other)
            {            
                return (*this > other) || (*this == other);
            }

            constexpr auto operator<=(const DataType& other)
            {            
                return !(*this > other);
            }

            constexpr auto operator<(const DataType& other)
            {            
                return !(*this >= other);
            }
        #endif
    };

    template<class Tag, class DataType, class DataType2>
    constexpr auto operator*(const DataType2& val, const Unit<Tag, DataType>& other)
    {
        return other * static_cast<DataType>(val);
    }

    template<class Tag, class DataType>
    constexpr auto operator/(const DataType& val, const Unit<Tag, DataType>& other)
    {
        using return_type = decltype(unittag::inverse_unittag(Tag {}));
        return Unit<return_type, DataType>(static_cast<DataType>(val) / other.value);
    }

    #ifdef RELAX_OPERATIONS
        template<class Tag, class DataType, class DataType2>
        constexpr auto operator+(const DataType2& val, const Unit<Tag, DataType>& other)
        {
            return other + static_cast<DataType>(val);
        }

        template<class Tag, class DataType, class DataType2>
        constexpr auto operator+(
            const Unit<unittag::UnitTag<unittag::NoTag>, DataType2>& f, 
            const Unit<Tag, DataType>& other
        )
        {
            return other + static_cast<DataType>(f.value);
        }

        template<class Tag, class DataType, class DataType2>
        constexpr auto operator-(const DataType2& val, const Unit<Tag, DataType>& other)
        {
            return -other + static_cast<DataType>(val);
        }

        template<class Tag, class DataType, class DataType2>
        constexpr auto operator-(
            const Unit<unittag::UnitTag<unittag::NoTag>, DataType2>& f, 
            const Unit<Tag, DataType>& other
        )
        {
            return other - static_cast<DataType>(f.value);
        }
    #endif

    template<class Tag, class DataType = float>
    constexpr auto unit_from_alias(const DataType& val)
    {
        return Unit<Tag, DataType> { val };
    }

    template<class Tag, int C = 1, class DataType = float>
    constexpr auto make_unit(const DataType& val)
    {
        using tag = unittag::UnitTag<unittag::UnitTagBase<Tag, C>>;
        return Unit<tag, DataType> { val };
    }

    template<class Tag, int C = 1, class DataType = float>
    constexpr auto make_inv_unit(const DataType& val)
    {
        using tag = unittag::UnitTag<unittag::UnitTagBase<Tag, -C>>;
        return Unit<tag, DataType> { val };
    }


};
