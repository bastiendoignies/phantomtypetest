#pragma once

#include <iostream>

namespace utils
{
    template<typename T>
    void print_func(T p)
    {
        std::cout << __PRETTY_FUNCTION__ << std::endl;
    }
    // Following code for filtering is taken on stackoverflow 

    // Store list of type in its template arguments
    template <typename...>
    struct Type_list {};
    // Concatenate two Type_list
    template <typename... T1s>
    constexpr auto concatenate(Type_list<T1s...>) 
    { return Type_list<T1s...>{}; }

    template <typename... T1s, typename... T2s, typename... TypeLists>
    constexpr auto concatenate(Type_list<T1s...>, Type_list<T2s...>, TypeLists...) 
    { return concatenate(Type_list<T1s..., T2s...>{}, TypeLists{}...); }


    // End of filtering function
    template <template <typename> typename Condition, typename Result>
    constexpr auto filter_types(Result result, Type_list<>) 
    {  return result; }

    // Filter types <T, Ts...> based on Condition
    // Result is the returning type;
    template <template <typename> typename Condition, typename Result, typename T, typename... Ts>
    constexpr auto filter_types(Result result, Type_list<T, Ts...>) 
    {
        if constexpr (Condition<T>{})
            return filter_types<Condition>(concatenate(result, Type_list<T>{}), Type_list<Ts...>{});
        else
            return filter_types<Condition>(result, Type_list<Ts...>{});
    }

    // Helper to get back types once filtered
    template <template <typename> typename Condition, typename... Types>
    using filtered_types = std::decay_t<decltype(filter_types<Condition>(Type_list<>{}, Type_list<Types...>{}))>;

    // Base class for removing a typelist, does nothing...
    template <class T>
    struct remove_typelist
    { using type = T; };

    // Partial specialisation.
    template<template<typename> class T, class... Args>
    struct remove_typelist<T<Type_list<Args...>>>
    { using type = T<Args...>;  };

    // Swap Type_list with another arbitrary type
    template<template<typename> class T, class... ArgsT,  class... Args>
    constexpr auto swap_typelist(T<ArgsT...>, Type_list<Args...>)
    { return T<Args...>{}; }

} // utils