#include "units.h"
#include <iostream>

#define TO_STRING(x) #x
using namespace units;

// Define Tags

// Meter
struct m_tag  {static constexpr std::string_view repr() { return "m"; }};
// Second
struct s_tag  {static constexpr std::string_view repr() { return "s"; }};
// Kilogram
struct kg_tag {static constexpr std::string_view repr() { return "kg"; }};
// Kelvin
struct K_tag  {static constexpr std::string_view repr() { return "K"; }};

// Note: numbers does not matters, only unit do 
using m  = decltype(make_unit<m_tag>(1.f));
using s  = decltype(make_unit<s_tag>(1.f));
using hz = decltype(make_unit<s_tag, -1>(1.f));
using K  = decltype(make_unit<K_tag>(1.f));
using kg = decltype(make_unit<kg_tag>(1.f));

// Alias type for Newton
using N = decltype(m() * kg() / (s() * s()));

int main(int argc, char** argv)
{
    std::cout << std::boolalpha;
    s  test1 = 1.f;
    hz test2 = 2.f;
    N  test3 = 3.f;

    std::cout << (test1 * test2).is_unitless() << std::endl;
    std::cout << test3 << std::endl;
    std::cout << (test3 == (test1 * test2)) << std::endl; // Test if unitless equals units

    return 0;
}