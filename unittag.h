#pragma once

#include <type_traits>
#include "utils.h"

#include <iostream>

namespace unittag
{
    // Base class to represent UnitTag
    // T is the UnitTagType while C is the power
    // Examples: 
    //      Area = UnitTagBase<Kilometers, 2>
    //      Hz   = UnitTagBase<Hz, -1>
    template<class T, int C>
    struct UnitTagBase
    {
        using UnitTagType = T;
        static constexpr auto Count() { return C; }
    };

    struct no_tag {static constexpr std::string_view repr() { return ""; }};
    using NoTag = UnitTagBase<no_tag, 0>;
    // Base class for UnitTag
    // A UnitTag is basically a list of UnitTagBase
    // This allows complex construction from arbitrary Tags
    // Examples:
    //      Given UnitBaseTag 
    //          Mass     = <Kg, 1>
    //          Distance = <M , 1>
    //          ISecondS = <S, -2>
    // Newton = <Mass, Distance, ISecondS>
    template<class UBf = NoTag, class... UBs>
    struct UnitTag {};

    // Check if two UnitTagBase shares the same UnitTagType
    // This ignores the power
    template<class UB1, class UB2>
    struct is_same_unittag_base_type :
        std::is_same<typename UB1::UnitTagType, typename UB2::UnitTagType> { };

    // Check if a given UnitTagType is in the provided list
    template<class UB1, class UBf, class... UBs>
    struct has_unittag_type : std::disjunction<
            is_same_unittag_base_type<UB1, UBf>,
            is_same_unittag_base_type<UB1, UBs> ...
        >
    { };
    
    // Check if a given UnitTag has a UnitTagBase (including power)
    template<class UB1, class UBf, class... UBs>
    struct has_unittag : std::disjunction<
        std::is_same<UB1, UBf>,
        std::is_same<UB1, UBs>...
    > {};

    // Check if all UnitTagBase of a Unit are in another
    // This function is not symmetric !
    template<class UBf1, class... UBs1,
             class UBf2, class... UBs2>
    constexpr bool is_first_unittag_included(UnitTag<UBf1, UBs1...> u1, 
                                             UnitTag<UBf2, UBs2...> u2)
    {
        return std::conjunction_v<
            has_unittag<UBf1, UBf2, UBs2...>,
            has_unittag<UBs1, UBf2, UBs2...>...
        >;
    }

    // Check if two UnitTags are the same (up to order of UnitTagBase)
    template<class UBf1, class... UBs1,
             class UBf2, class... UBs2>
    constexpr bool is_same_unittag(UnitTag<UBf1, UBs1...> u1, 
                                   UnitTag<UBf2, UBs2...> u2)
    {
        return is_first_unittag_included(u1, u2) && 
               is_first_unittag_included(u2, u1);
    }

    // Check if a UnitTagBase can be removed from the list
    // A UnitTagBase can be removed if its power is null or its base type is void
    template<class UB>
    struct is_unittagbase_removable : std::disjunction< 
        std::bool_constant<bool(UB::Count())>,
        std::is_same<typename UB::UnitTagType, void>
    >
    {};

    // Invese a unittype
    template<class UBf1, class... UBs1>
    constexpr auto inverse_unittag(UnitTag<UBf1, UBs1...> u)
    {
        return UnitTag<UnitTagBase<typename UBf1::UnitTagType, -UBf1::Count()>,
                       UnitTagBase<typename UBs1::UnitTagType, -UBs1::Count()>...>{}; 
    } 

    // Simplify a UnitTag, removing all non essential components
    template<class UBf1, class... UBs1>
    constexpr auto simplified_unittag(UnitTag<UBf1, UBs1...> u)
    {
        using filtered = utils::filtered_types<is_unittagbase_removable, UBf1, UBs1...>;
        return utils::swap_typelist(UnitTag<NoTag>{}, filtered {});
    }

    template<class UB1, class UB2>
    struct CombineIfSameUnitTagBase
    { 
        using type = std::conditional_t<is_same_unittag_base_type<UB1, UB2>::value, 
                                        UnitTag<UnitTagBase<typename UB1::UnitTagType, UB1::Count() + UB2::Count()>>,
                                        UnitTag<UB1, UB2>>;
    };

    // Combine to UnitTagBase in a multiplicative sense
    // If both have the same UnitTagBaseType, then the 
    // powers are just added, 
    // Otherwise, just keep the second parameter
    // Example: 
    //      CombineIfSameUnit<UnitTagBase<S, -1>, UnitTagBase<S, -1>> = UnitTagBase<S, -2>
    //      CombineIfSameUnit<UnitTagBase<S, -1>, UnitTagBase<K, -1>> = UnitTagBase<K, -1>  
    template<class UB1, class UB2>
    struct CombineIfSameUnitTag
    { 
        using type = std::conditional_t<is_same_unittag_base_type<UB1, UB2>::value, 
                                        UnitTagBase<typename UB1::UnitTagType, UB1::Count() + UB2::Count()>,
                                        UB2>;
    };

    // Combine a UnitTagBase with a UnitTag
    // type holds a Type_list to allow further operation
    template<class UB, class UBf = NoTag, class... UBs>
    struct CombinedUnitTag
    {
        using type = std::conditional_t<
                        !has_unittag_type<UB, UBf, UBs...>::value,
                        utils::Type_list<UB, UBf, UBs...>,
                        utils::Type_list<
                            typename CombineIfSameUnitTag<UB, UBf>::type, 
                            typename CombineIfSameUnitTag<UB, UBs>::type...>
                    >;
    };

    // Creates a unit from the combination of a UnitTagBase and a UnitTag
    template<class UB, class UBf, class... UBs>
    constexpr auto combine_unittagbase_and_unit(UB ub, UnitTag<UBf, UBs...> unit)
    {
        return typename utils::remove_typelist<UnitTag<typename CombinedUnitTag<UB, UBf, UBs...>::type>>::type();
    }

    // Combine two units in a multiplicative sense
    // Requires UBs1 and UBs2 not to be empty as well as 
    // sizeof...(UBs1) > sizeof...(UBs2)
    template<class UBf1, class... UBs1,
             class UBf2, class... UBs2>
    constexpr auto combine_unittags(UnitTag<UBf1, UBs1...> u1, UnitTag<UBf2, UBs2...> u2)
    {
        auto types = combine_unittagbase_and_unit(UBf1{}, UnitTag<UBf2, UBs2...>{});
        return combine_unittags(UnitTag<UBs1...>{}, types); 
    }

    template<class UBf1,
             class UBf2, class... UBs2>
    constexpr auto combine_unittags(UnitTag<UBf1> u1, UnitTag<UBf2, UBs2...> u2)
    {
        auto types = combine_unittagbase_and_unit(UBf1{}, UnitTag<UBf2, UBs2...>{});
        return types; 
    }

    template<class Stream, class UBf1, class... UBs1>
    constexpr Stream& unit_print(Stream& s, UnitTag<UBf1, UBs1...> u)
    {   
        if constexpr (std::is_same_v<UBf1, NoTag>)
        {
            return unit_print(s, UnitTag<UBs1...>{});
        }
        if constexpr (UBf1::Count() == 1) 
        {
            s << UBf1::UnitTagType::repr() << ".";
            return unit_print(s, UnitTag<UBs1...>{});
        }

        s << UBf1::UnitTagType::repr() << "^" << UBf1::Count() << ".";
        return unit_print(s, UnitTag<UBs1...>{});
    }

    template<class Stream, class UBf1>
    constexpr Stream& unit_print(Stream& s, UnitTag<UBf1> u)
    {   
        if constexpr (std::is_same_v<UBf1, NoTag>)
        {
            return s;
        }
        if constexpr (UBf1::Count() == 1) 
        {
            s << UBf1::UnitTagType::repr();
            return s;
        }

        s << UBf1::UnitTagType::repr() << "^" << UBf1::Count();
        return s;
    }
};